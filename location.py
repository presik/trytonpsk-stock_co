# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval
from trytond.transaction import Transaction
from trytond.modules.product import price_digits


class ProductsByLocations(metaclass=PoolMeta):
    __name__ = 'stock.products_by_locations'
    brand = fields.Function(fields.Many2One('product.brand', 'Brand'), 'get_brand')
    reference = fields.Function(fields.Char('Reference'), 'get_reference')
    list_price = fields.Function(fields.Numeric("List Price", digits=price_digits), 'get_list_price')

    def get_brand(self, name):
        if self.product.brand:
            return self.product.brand.id

    def get_reference(self, name):
        return self.product.reference

    def get_list_price(self, name):
        return self.product.list_price
