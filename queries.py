
consolidated_kardex = """
    SELECT
        mo.product AS id,
        pr.code AS code,
        tm.name AS name,
        tm.reference,
        uom.symbol AS unit,
    ROUND(SUM(CASE
        WHEN to_location={location_id} AND effective_date<'{from_date}' THEN mo.quantity
        WHEN from_location={location_id} AND effective_date<'{from_date}' THEN -mo.quantity END)::NUMERIC, 2
      ) AS start_balance,
    ROUND(SUM(
        CASE WHEN to_location={location_id} AND effective_date>='{from_date}' THEN mo.quantity ELSE 0 END)::NUMERIC, 2
      ) AS input,
    ROUND(SUM(
        CASE WHEN from_location={location_id} AND effective_date>='{from_date}' THEN mo.quantity ELSE 0 END)::NUMERIC
      ) AS output,
    ROUND(SUM(CASE
        WHEN to_location={location_id} AND effective_date<='{to_date}' THEN mo.quantity
        WHEN from_location={location_id} AND effective_date<='{to_date}' THEN -mo.quantity END)::NUMERIC, 2
    ) AS end_balance
FROM stock_move AS mo
JOIN product_product AS pr ON pr.id=mo.product
JOIN product_template AS tm ON pr.template=tm.id
JOIN product_uom AS uom ON tm.default_uom=uom.id
WHERE effective_date<='{to_date}' AND state IN ('assigned', 'done')
  AND (to_location={location_id} OR from_location={location_id})
  GROUP BY mo.product, pr.code, tm.name, tm.reference, uom.symbol
  ORDER BY tm.name
"""

start_kardex = """
    SELECT
        mo.product AS product_id,
        ROUND(SUM(
            CASE
                WHEN to_location={location_id} AND effective_date<'{from_date}' THEN mo.quantity
                WHEN from_location={location_id} AND effective_date<'{from_date}' THEN -mo.quantity END
            )::NUMERIC, 2
        ) AS start_balance
    FROM stock_move AS mo
    WHERE effective_date<'{from_date}' AND (to_location={location_id}
        OR from_location={location_id}) AND state IN ('assigned', 'done')
      {in_products}
      GROUP BY mo.product
 """

cost_kardex = """
    SELECT
        product,
        cost_price,
        effective_date
    FROM product_average_cost AS avg
    WHERE effective_date<'{to_date}' {in_products}
    ORDER BY effective_date ASC
 """

by_move_kardex = """
    SELECT
        mo.id AS move_id,
        mo.product AS product_id,
        pr.code AS code,
        tm.name AS product,
        tm.reference AS reference,
        uom.symbol AS unit,
        orig.cost AS input_cost,
        CASE WHEN mo.origin ILIKE 'sale.line%' THEN mo.unit_price ELSE 0 END AS output_cost,
        mo.effective_date AS effective_date,
        CASE WHEN shipment IS NULL THEN mo.origin ELSE mo.shipment END AS origin,
        CASE WHEN shipment IS NULL THEN SPLIT_PART(mo.origin, ',', 1) ELSE SPLIT_PART(mo.shipment, ',', 1) END AS model,
        CASE WHEN shipment IS NULL THEN SPLIT_PART(mo.origin, ',', 2) ELSE SPLIT_PART(mo.shipment, ',', 2) END AS origin_id,
        0 AS start_balance,
        CASE WHEN to_location={to_location} THEN mo.quantity ELSE NULL END AS input,
        CASE WHEN from_location={from_location} THEN mo.quantity ELSE NULL END AS output,
        0 AS end_balance
    FROM stock_move AS mo
    JOIN product_product AS pr ON pr.id=mo.product
    JOIN product_template AS tm ON pr.template=tm.id
    JOIN product_uom AS uom ON tm.default_uom=uom.id
    LEFT JOIN (
      SELECT
        origin,
        unit_price AS cost,
        'stock.move,' || id AS origin_id
      FROM stock_move
      WHERE effective_date>='{from_date}' AND effective_date<='{to_date}'
          AND to_location={input_location}
          AND state IN ('assigned', 'done')
          {in_products}
    ) AS orig ON orig.origin_id=mo.origin
    WHERE effective_date>='{from_date}' AND effective_date<='{to_date}'
      AND state IN ('assigned', 'done') AND (
        to_location={to_location} OR from_location={from_location}
      ) {in_products}
    ORDER BY mo.effective_date ASC, mo.id ASC
"""
