select p.id as pid, p.code, t.name, pb.name as brand, t.reference, s.effective_date, lp.list_price,
SPLIT_PART(s.origin, ',', 1) as model,
SPLIT_PART(s.origin, ',', 2) as line_id,
s.quantity as quantity
from stock_move as s
INNER JOIN product_product as p on p.id = s.product
INNER JOIN product_template as t on p.template = t.id
INNER JOIN product_list_price as lp on t.id = lp.template
INNER JOIN product_brand as pb on pb.id = t.brand
where s.effective_date >= '2023-10-01'
and s.effective_date <= '2023-10-31'
and (s.origin LIKE '%purchase.line%' or s.origin LIKE '%sale.line%')
AND p.id IN (12394);


select t1.pid, t1.model, t1.line_id, slt.number, slt.party,
t1.id AS pid, t1.code, t1.name, t1.name AS brand, t1.reference, t1.effective_date, t1.list_price,
from t1  
INNER JOIN (
    SELECT line.id, sale.number, sale.party 
    FROM sale_line AS line
    INNER JOIN sale_sale AS sale ON line.sale=sale.id 
) AS slt ON slt.id=t1.line_id  
WHERE t1.model='sale.line'
INNER JOIN (
    SELECT line.id, purchase.number, purchase.party 
    FROM purchase_line AS line
    INNER JOIN purchase_purchase AS purchase ON line.purchase=purchase.id 
) AS slt ON slt.id=t1.line_id  
WHERE t1.model='purchase.line'






select t1.pid, t1.model, t1.line_id, slt.number, slt.party,
select t1.pid, t1.model, t1.line_id, t1.code, t1.name, t1.name AS brand, 
t1.reference, t1.effective_date, t1.quantity,
slt.number as sale_number, slt.unit_price
FROM 
(select p.id as pid, p.code, t.name, pb.name as brand, t.reference, s.effective_date,
SPLIT_PART(s.origin, ',', 1) as model,
SPLIT_PART(s.origin, ',', 2) as line_id,
s.quantity as quantity
from stock_move as s
INNER JOIN product_product as p on p.id = s.product
INNER JOIN product_template as t on p.template = t.id
INNER JOIN product_brand as pb on pb.id = t.brand
where s.effective_date >= '2023-10-01'
and s.effective_date <= '2023-10-31'
and (s.origin LIKE '%purchase.line%' or s.origin LIKE '%sale.line%')
AND p.id IN (12394)) as t1
INNER JOIN (
    SELECT line.id, line.unit_price, sale.number, sale.party, 'sale.line' AS model
    FROM sale_line AS line
    INNER JOIN sale_sale AS sale ON line.sale=sale.id
) AS slt ON slt.id=CAST(t1.line_id AS integer)
INNER JOIN (
    SELECT line.id, line.unit_price, purchase.number, purchase.party 
    FROM purchase_line AS line
    INNER JOIN purchase_purchase AS purchase ON line.purchase=purchase.id 
) AS plt ON plt.id=CAST(t1.line_id AS integer) 
WHERE t1.model='purchase.line';
WHERE t1.model='sale.line'



