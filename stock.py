# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import re
from datetime import date, datetime, timedelta
from decimal import Decimal
import polars as pol

from trytond.model import ModelView, fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Bool, Equal, Eval, If, In, Not
from trytond.report import Report
from trytond.transaction import Transaction
from trytond.wizard import Button, StateReport, StateTransition, StateView, Wizard
from .helper import polar_query
from .queries import by_move_kardex, consolidated_kardex, start_kardex, cost_kardex

STATES = {'invisible': (Eval('type') != 'goods')}

STATES_MOVE = {
    'readonly': Eval('state').in_(['cancel', 'assigned', 'done']),
}

MAP_FIELDS = {
    'sale.line': {
        'number': 'sale.number',
        'party': 'sale.party.name',
    },
    'purchase.line': {
        'number': 'purchase.number',
        'party': 'purchase.party.name',
    },
    'stock.inventory.line': {
        'number': 'inventory.number',
    },
    'production': {
        'number': 'production.number'
    },
    'stock.shipment.in': {
        'number': 'number',
        'party': 'supplier.name',
    },
    'stock.shipment.out': {
        'number': 'number',
        'party': 'customer.name',
    },
    'stock.shipment.internal': {
        'number': 'number',
    },
}


class Lot(metaclass=PoolMeta):
    __name__ = 'stock.lot'
    active = fields.Boolean('Active')

    @staticmethod
    def default_active():
        return True


class Move(metaclass=PoolMeta):
    __name__ = 'stock.move'
    description = fields.Char('Description')
    current_stock = fields.Function(fields.Float('Current Stock',
        depends=['product']), 'on_change_with_current_stock')
    reference = fields.Function(fields.Char('Reference',
        depends=['product'], help='reference of product'), 'get_reference')

    @fields.depends('current_stock')
    def on_change_product(self, name=None):
        super(Move, self).on_change_product()
        self.current_stock = self.on_change_with_current_stock()

    @fields.depends('product')
    def get_reference(self, name=None):
        reference = None
        if self.product and hasattr(self.product, 'reference') and self.product.reference:
            reference = self.product.reference
        return reference

    @fields.depends('product', 'from_location', 'to_location')
    def on_change_with_current_stock(self, name=None):
        res = 0
        location = None
        if self.from_location and self.from_location.type == 'storage' and self.from_location.parent:
            location = self.from_location.parent.storage_location
        elif self.to_location and self.to_location.type == 'storage' and self.to_location.parent:
            location = self.to_location.parent.storage_location
        if self.product and location:
            context = {
                'location_ids': [location.id],
                'stock_date_end': date.today(),
            }
            with Transaction().set_context(context):
                res_dict = self.product._get_quantity(
                    [self.product],
                    'quantity',
                    [location.id],
                    grouping_filter=([self.product.id],),
                )
                if res_dict.get(self.product.id):
                    res += res_dict[self.product.id]
        return res

    @fields.depends('product')
    def on_change_with_description(self, name=None):
        res = ''
        if self.product and self.product.description:
            description = self.product.description
            res = re.sub(r"\n", "/", description)
        return res

    @classmethod
    def do(cls, moves):
        super(Move, cls).do(moves)
        for move in moves:
            product = move.product
            if not move.description:
                move.description = cls.on_change_with_description(move)
                move.save()
            # remove origin 'stock.inventory.line' for set average cost
            if move.origin and move.origin.__name__ in ('purchase.line'):
                if product.cost_price_method == 'fixed':
                    product.cost_price = move.unit_price
                    product.save()
                move.set_average_cost()
                if hasattr(product, 'standard_margin') and product.standard_margin and product.standard_margin > 0 and move.product.cost_price > 0:
                    template = product.template
                    percentage_calculation = round(move.product.cost_price * Decimal(move.product.standard_margin / 100), 4)
                    template.list_price = product.cost_price + percentage_calculation
                    template.on_change_list_price()
                    template.save()

    def set_average_cost(self):
        AverageCost = Pool().get('product.average_cost')
        data = {
            "stock_move": self.id,
            "product": self.product.id,
            "effective_date": self.effective_date,
            "cost_price": self.product.cost_price,
        }
        AverageCost.create([data])


class MoveByProductStart(ModelView):
    "Move By Product Start"
    __name__ = 'stock_co.move_by_product.start'
    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date', required=True)
    from_location = fields.Many2One('stock.location', 'From Location')
    to_location = fields.Many2One('stock.location', 'To Location',
        required=True)
    categories = fields.Many2Many('product.category', None, None, 'Categories')
    company = fields.Many2One('company.company', 'Company',
            required=True)
    party = fields.Many2One('party.party', 'Party')
    shipment_draft = fields.Boolean('Shipment Draft')
    start_time = fields.Time('Start Time')
    end_time = fields.Time('End Time', states={
        'required': Bool(Eval('start_time')),
        'invisible': Not(Bool(Eval('start_time'))),
    }, depends=['start_time'])
    product = fields.Many2One('product.product', 'Product')
    brand = fields.Many2One('product.brand', 'Brand')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_end_date():
        Date_ = Pool().get('ir.date')
        return Date_.today()

    @staticmethod
    def default_start_date():
        Date_ = Pool().get('ir.date')
        return Date_.today()


class PrintMoveByProduct(Wizard):
    "Move By Product"
    __name__ = 'stock_co.print_move_by_product'
    start = StateView('stock_co.move_by_product.start',
        'stock_co.print_move_by_product_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-print', default=True),
        ])
    print_ = StateReport('stock_co.move_by_product')

    def do_print_(self, action):
        category_ids = []
        from_location_id = None
        brand_id = None
        party_id = None
        start_time = None
        end_time = None

        if self.start.from_location:
            from_location_id = self.start.from_location.id
        if self.start.categories:
            category_ids = [acc.id for acc in self.start.categories]
        if self.start.brand:
            brand_id = self.start.brand.id
        if self.start.party:
            party_id = self.start.party.id
        if self.start.start_time:
            start_time = self.start.start_time
        if self.start.end_time:
            end_time = self.start.end_time

        data = {
            'company': self.start.company.id,
            'from_location': from_location_id,
            'to_location': self.start.to_location.id,
            'start_date': self.start.start_date,
            'end_date': self.start.end_date,
            'categories': category_ids,
            'brand': brand_id,
            'party': party_id,
            'shipment_draft': self.start.shipment_draft,
            'start_time': start_time,
            'end_time': end_time,
            'product': self.start.product.id if self.start.product else None,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class MoveByProduct(Report):
    "Move By Product Report"
    __name__ = 'stock_co.move_by_product'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Company = pool.get('company.company')
        Move = pool.get('stock.move')
        Location = pool.get('stock.location')
        Party = pool.get('party.party')
        Category = pool.get('product.template-product.category')
        company = Company(data['company'])
        from_location = None
        brand_name = ''
        party_name = ''
        start_date = None
        end_date = None

        categories = Category.search([('category', 'in', data['categories'])])

        products_ids = [c.template.id for c in categories]
        dom_products = []
        if data['start_time']:
            start_date = datetime.combine(data['start_date'], data['start_time'])
            end_date = datetime.combine(data['end_date'], data['end_time'])

            _start_date = Company.convert_timezone(start_date, True)
            _end_date = Company.convert_timezone(end_date, True)
            dom_products.append(
                ('to_location.id', '=', data['to_location']),
                ('create_date', '>=', _start_date),
                ('create_date', '<=', _end_date),
            )
        else:
            start_date = data['start_date']
            end_date = data['end_date']

            dom_products.extend([
                ('to_location', '=', data['to_location']),
                ['AND',
                ['OR', [
                        ('planned_date', '>=', start_date),
                        ('planned_date', '<=', end_date),
                    ], [
                        ('effective_date', '>=', start_date),
                        ('effective_date', '<=', end_date),
                    ],
                ]],
            ])

        if data['shipment_draft']:
            dom_products.append(('state', '=', 'draft'))

        if data['from_location']:
            dom_products.append(
                ('from_location.id', '=', data['from_location']),
            )
            from_location = Location(data['from_location'])
        if data['categories']:
            if products_ids:
                dom_products.append(
                    ('product.template', 'in', products_ids),
                )
            else:
                dom_products.append(
                    ('product.template.account_category', 'in', data['categories']),
                )
        if data['brand']:
            Brand = pool.get('product.brand')
            dom_products.append(
                ('product.template.brand', '=', data['brand']),
            )
            brand_name = Brand(data['brand']).name
        if data['party']:
            dom_products.append(
                ('invoice_lines.invoice.party', '=', data['party']),
            )
            party_name = Party(data['party']).name
        if data['product']:
            dom_products.append(
                ('product', '=', data['product']),
            )
        moves = Move.search([dom_products])

        products = {}
        Location = pool.get('stock.location')
        location = Location(data['to_location'])
        for move in moves:
            product = move.product
            amount = move.quantity * float(product.cost_price)
            origin = None
            id_dict = product.id
            if location.type == 'customer':
                id_dict = move.origin.sale.party.name + str(product.id)
                origin = move.origin.sale.party.name if move.origin else None
            try:
                row = products[id_dict]
                row[4].append(move.quantity)
                row[6].append(amount)
            except Exception:
                template = product.template
                brand = template.brand and template.brand.name
                category = template.account_category and template.account_category.name
                effective_date = move.effective_date
                products[id_dict] = [
                    product.code,
                    origin,
                    product.rec_name,
                    template.default_uom.symbol,
                    [move.quantity],
                    product.cost_price,
                    [amount],
                    category,
                    brand,
                    template.list_price,
                    effective_date,
                ]

        report_context['records'] = products.values()
        report_context['from_location'] = from_location
        report_context['to_location'] = location
        report_context['customer_location'] = location.type == 'customer'
        report_context['start_date'] = start_date
        report_context['end_date'] = end_date
        report_context['company'] = company
        report_context['brand_name'] = brand_name
        report_context['party_name'] = party_name
        return report_context


class WarehouseStockStart(ModelView):
    "Warehouse Stock Start"
    __name__ = 'stock_co.warehouse_stock.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    locations = fields.Many2Many('stock.location', None, None, "Locations",
        domain=[
            ('type', 'in', ['storage', 'customer']),
            ('active', '=', True),
        ], required=True)
    to_date = fields.Date('To Date', required=True)
    only_minimal_level = fields.Boolean('Only Minimal Level')
    group_by_location = fields.Boolean('Group By Location')
    group_by_supplier = fields.Boolean('Group By Supplier')
    category = fields.Many2One('product.category', 'Category')
    suppliers = fields.Many2Many('party.party', None, None, "Suppliers",
        domain=[
            ('active', '=', True),
        ])
    zero_quantity = fields.Boolean('Zero Quantity')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_to_date():
        Date_ = Pool().get('ir.date')
        return Date_.today()


class WarehouseStock(Wizard):
    "Warehouse Stock"
    __name__ = 'stock_co.warehouse_stock'
    start = StateView('stock_co.warehouse_stock.start',
        'stock_co.warehouse_stock_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
        ])
    print_ = StateReport('stock_co.warehouse_stock.report')

    def do_print_(self, action):
        category_id = None
        if self.start.category:
            category_id = self.start.category.id
        data = {
            'company': self.start.company.id,
            'locations': [l.id for l in self.start.locations],
            'location_names': ', '.join([l.name for l in self.start.locations]),
            'only_minimal_level': self.start.only_minimal_level,
            'group_by_location': self.start.group_by_location,
            'group_by_supplier': self.start.group_by_supplier,
            'zero_quantity': self.start.zero_quantity,
            'to_date': self.start.to_date,
            'category': category_id,
            'suppliers': [l.id for l in self.start.suppliers],
        }
        return action, data


class WarehouseReport(Report):
    "Warehouse Report"
    __name__ = 'stock_co.warehouse_stock.report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Company = pool.get('company.company')
        Product = pool.get('product.product')
        OrderPoint = pool.get('stock.order_point')
        Location = pool.get('stock.location')
        ids_location = data['locations']
        locations = Location.browse(data['locations'])
        Move = pool.get('stock.move')
        dom_products = [
            ('active', '=', True),
            ('template.active', '=', True),
            ('type', '=', 'goods'),
            ('consumable', '=', False),
        ]

        stock_context = {
            'stock_date_end': data['to_date'],
            'locations': ids_location,
        }

        if data['category']:
            dom_products.append(['AND', ['OR', [
                    ('account_category', '=', data['category']),
                ], [
                    ('categories', 'in', [data['category']]),
                ],
            ]])
        if not data['zero_quantity']:
            dom_products.append([('quantity', '!=', 0)])

        if data['only_minimal_level']:
            locations = Location.search([('id', 'in', ids_location)])
            location_parents = [location.parent.id for location in locations]
            order_points_wl = OrderPoint.search([
                ('warehouse_location', 'in', set(location_parents)),
                ('type', '=', 'purchase'),
            ])
            min_quantities = {op.product.id: op.min_quantity for op in order_points_wl}
            order_points_sl = OrderPoint.search([
                ('storage_location', 'in', ids_location),
                ('type', '=', 'internal'),
            ])
            min_quantities.update({op.product.id: op.min_quantity for op in order_points_sl})
            products_ids = min_quantities.keys()
            dom_products.append(('id', 'in', products_ids))
        if data['suppliers']:
            dom_products.append([('template.product_suppliers.party', 'in', data['suppliers'])])

        total_amount = 0
        total_amount_imp = 0
        values = {}
        products = []
        if data['group_by_location']:
            for l in locations:
                days = []
                stock_context['locations'] = [l.id]
                with Transaction().set_context(stock_context):
                    prdts = Product.search(dom_products, order=[('code', 'ASC')])
                suppliers = {}
                for p in prdts:
                    move = Move.search([
                        ('to_location', '=', l.id),
                        ('product', '=', p.id),
                        ('state', '=', 'done'),
                    ], order=[('effective_date', 'DESC')])
                    date_start = move[0].effective_date if len(move) > 0 else None
                    date_now = date.today()
                    date_result = date_now - date_start if date_start else 0
                    days.append(date_result.days) if date_start else days.append('error')
                if data['group_by_supplier']:
                    for p in prdts:
                        if not p.template.product_suppliers:
                            continue
                        for prod_sup in p.template.product_suppliers:
                            sup_id = prod_sup.party.id
                            try:
                                suppliers[sup_id]['products'].append(p)
                                suppliers[sup_id]['total_amount'].append(p.amount_cost if p.amount_cost else 0)
                                suppliers[sup_id]['total_amount_imp'].append(p.extra_tax * Deciaml(p.quantity) if p.extra_tax else 0)
                            except:
                                suppliers[sup_id] = {}
                                suppliers[sup_id]['products'] = [p]
                                suppliers[sup_id]['party'] = prod_sup.party
                                suppliers[sup_id]['total_amount'] = [p.amount_cost if p.amount_cost else 0]
                                suppliers[sup_id]['total_amount_imp'] = [p.extra_tax * Decimal(p.quantity) if p.extra_tax else 0]
                total_amount = sum([p.amount_cost for p in prdts if p.amount_cost])
                total_amount_imp = sum([p.extra_tax * Decimal(p.quantity) for p in prdts if p.extra_tax])
                values[l.id] = {
                    'name': l.name,
                    'products': prdts,
                    'days': days,
                    'suppliers': suppliers.values(),
                    'total_amount': total_amount,
                    'total_amount_imp': total_amount_imp,
                }
            products = values.values()
        else:
            with Transaction().set_context(stock_context):
                products = Product.search(dom_products, order=[('code', 'ASC')])

            if data['only_minimal_level']:
                products = [p for p in products if p.quantity <= min_quantities[p.id]]
            total_amount = sum([p.amount_cost for p in products if p.amount_cost])
            total_amount_imp = sum([p.extra_tax * Decimal(p.quantity) for p in products if p.extra_tax])
            suppliers = {}
            if data['group_by_supplier']:
                for p in products:
                    if not p.template.product_suppliers:
                        continue
                    for prod_sup in p.template.product_suppliers:
                        sup_id = prod_sup.party.id
                        try:
                            suppliers[sup_id]['products'].append(p)
                            suppliers[sup_id]['total_amount'].append(p.amount_cost if p.amount_cost else 0)
                            suppliers[sup_id]['total_amount_imp'].append(p.extra_tax * Decimal(p.quantity) if p.extra_tax else 0)
                        except:
                            suppliers[sup_id] = {}
                            suppliers[sup_id]['products'] = [p]
                            suppliers[sup_id]['party'] = prod_sup.party
                            suppliers[sup_id]['total_amount'] = [p.amount_cost if p.amount_cost else 0]
                            suppliers[sup_id]['total_amount_imp'] = [p.extra_tax * Decimal(p.quantity) if p.extra_tax else 0]
                products = suppliers.values()

        cursor = Transaction().connection.cursor()
        # query = "select distinct on(p.id) p.id, t.name, p.code, s.effective_date from product_product as p right join stock_move as s on p.id=s.product join product_template as t on p.template=t.id where (date_part('day', TIMESTAMP '{}') - date_part('day', s.effective_date))>45 and s.shipment ilike 'stock.shipment.in,%' and state='done' order by p.id, s.effective_date DESC;".format(data['to_date'])
        query = "select distinct on(p.id) p.id, t.name, p.code, s.effective_date from product_product as p right join stock_move as s on p.id=s.product join product_template as t on p.template=t.id where s.shipment ilike 'stock.shipment.in,%' and state='done' order by p.id, s.effective_date DESC;"

        cursor.execute(query)
        columns = list(cursor.description)
        result = cursor.fetchall()
        last_purchase = {}

        for row in result:
            row_dict = {}
            for i, col in enumerate(columns):
                row_dict[col.name] = row[i]
            last_purchase[row[0]] = row_dict

        report_context['group_by_location'] = data['group_by_location']
        report_context['group_by_supplier'] = data['group_by_supplier']
        report_context['records'] = products
        report_context['total_amount'] = total_amount
        report_context['total_amount_imp'] = total_amount_imp
        report_context['last_purchase'] = last_purchase
        report_context['location'] = data['location_names']
        report_context['stock_date_end'] = data['to_date']
        report_context['company'] = Company(data['company'])
        return report_context


class PrintProductsStart(ModelView):
    "Products Start"
    __name__ = 'stock_co.print_products.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    category = fields.Many2One('product.category', 'Category')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class PrintProducts(Wizard):
    "Warehouse Stock"
    __name__ = 'stock_co.print_products'
    start = StateView('stock_co.print_products.start',
        'stock_co.print_products_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
        ])
    print_ = StateReport('stock_co.print_products.report')

    def do_print_(self, action):
        category_id = None
        if self.start.category:
            category_id = self.start.category.id
        data = {
            'company': self.start.company.id,
            'category': category_id,
        }
        return action, data


class PrintProductsReport(Report):
    "Warehouse Report"
    __name__ = 'stock_co.print_products.report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Company = pool.get('company.company')
        Product = pool.get('product.product')
        Date_ = Pool().get('ir.date')
        dom_products = [
            ('active', '=', True),
            ('type', '=', 'goods'),
            ('consumable', '=', False),
        ]

        if data['category']:
            dom_products.append(('account_category', '=', data['category']))

        products = Product.search(dom_products, order=[('code', 'ASC')])

        report_context['records'] = products
        report_context['stock_date_end'] = Date_.today()
        report_context['company'] = Company(data['company'])
        return report_context


class WarehouseStockDetailedStart(ModelView):
    "Warehouse Stock Detailed Start"
    __name__ = 'stock_co.warehouse_stock_detailed.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    locations = fields.Many2Many('stock.location', None, None, "Locations",
        domain=[
            ('id', 'in', Eval('location_storage')),
            ('active', '=', True),
        ], depends=['location_storage'])
    location_storage = fields.One2Many('stock.location', None, 'Locations Storage')
    to_date = fields.Date('To Date', required=True)
    lot = fields.Boolean('Grouping Lot')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_to_date():
        Date_ = Pool().get('ir.date')
        return Date_.today()

    @staticmethod
    def default_location_storage():
        Location = Pool().get('stock.location')
        locations = Location.search([('type', '=', 'warehouse')])
        return [l.storage_location.id for l in locations if l.storage_location.id]


class WarehouseStockDetailed(Wizard):
    "Warehouse Stock Detailed"
    __name__ = 'stock_co.warehouse_stock_detailed'
    start = StateView('stock_co.warehouse_stock_detailed.start',
        'stock_co.warehouse_stock_detailed_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
        ])

    print_ = StateReport('stock_co.warehouse_stock_detailed.report')

    def do_print_(self, action):
        location_ids = None
        if self.start.locations:
            location_ids = [l.id for l in self.start.locations]
        data = {
            'company': self.start.company.id,
            'locations': location_ids,
            'to_date': self.start.to_date,
            'lot': self.start.lot,
        }
        return action, data


class WarehouseStockDetailedReport(Report):
    "Warehouse Stock Detailed Report"
    __name__ = 'stock_co.warehouse_stock_detailed.report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Company = pool.get('company.company')
        Product = pool.get('product.product')
        Location = pool.get('stock.location')
        LotbyLocation = pool.get('stock.lots_by_locations')
        if data['locations']:
            ids_location = data['locations']
            locations = Location.browse(ids_location)
        else:
            locations = Location.search([
                ('type', '=', 'warehouse'),
            ])
            ids_location = [l.storage_location.id for l in locations if l.storage_location]
        dom_products = [
            ('active', '=', True),
            ('template.active', '=', True),
            ('type', '=', 'goods'),
            ('consumable', '=', False),
        ]

        stock_context = {
            'stock_date_end': data['to_date'],
            'locations': ids_location,
        }
        # dom_products.append(('account_category', '=', data['category']))

        cursor = Transaction().connection.cursor()
        query = "select distinct on(p.id) p.id, t.name, p.code, t.reference, s.effective_date from product_product as p right join stock_move as s on p.id=s.product join product_template as t on p.template=t.id where s.shipment ilike 'stock.shipment.in,%' and state='done' order by p.id, s.effective_date DESC"
        cursor.execute(query)
        columns = list(cursor.description)
        result = cursor.fetchall()
        last_purchase = {}

        for row in result:
            row_dict = {}
            for i, col in enumerate(columns):
                row_dict[col.name] = row[i]
            last_purchase[row[0]] = row_dict

        values = []
        add_ = values.append
        for l in locations:
            stock_context['locations'] = [l.id]
            with Transaction().set_context(stock_context):
                prdts = Product.search(dom_products, order=[('code', 'ASC')])
                if data['lot']:
                    prd_ids = [p.id for p in prdts]
                    prdts = LotbyLocation.search([('product', 'in', prd_ids)])
            for p in prdts:
                add_({
                    'parent_name': l.parent.name if l.parent else '',
                    'name': l.name,
                    'product': p.product if data['lot'] else p,
                    'lot': p if data['lot'] else None,
                })

        products = values
        report_context['records'] = products
        report_context['Decimal'] = Decimal
        report_context['last_purchase'] = last_purchase
        report_context['stock_date_end'] = data['to_date']
        report_context['company'] = Company(data['company'])
        return report_context


class WarehouseKardexStockStart(ModelView):
    "Warehouse Kardex Stock Start"
    __name__ = 'stock_co.warehouse_kardex_stock.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    location = fields.Many2One('stock.location', "Location",
        domain=[
            ('type', '=', 'warehouse'),
        ], required=True)
    from_date = fields.Date('From Date', required=True)
    to_date = fields.Date('To Date', required=True)
    kind = fields.Selection([
        ('consolidated', 'Consolidated'),
        ('by_move', 'By Move')
        ], 'Kind', required=True)
    categories = fields.Many2Many('product.category', None, None, 'Categories')
    products = fields.Many2Many('product.product', None, None, 'Products',
        domain=[
            ('active', '=', True),
            ('template.active', '=', True),
            ('type', '=', 'goods'),
            ('consumable', '=', False),
        ], states={
            'required': Eval('kind') == 'by_move'
        }, depends=['kind'])

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_to_date():
        Date_ = Pool().get('ir.date')
        return Date_.today()

    @staticmethod
    def default_kind():
        return 'consolidated'


class WarehouseKardexStock(Wizard):
    "Warehouse Kardex Stock"
    __name__ = 'stock_co.warehouse_kardex_stock'
    start = StateView(
        'stock_co.warehouse_kardex_stock.start',
        'stock_co.warehouse_kardex_stock_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
        ])
    print_ = StateReport('stock_co.warehouse_kardex_stock.report')

    def do_print_(self, action):
        data = {
            'company': self.start.company.id,
            'from_date': self.start.from_date,
            'to_date': self.start.to_date,
            'kind': self.start.kind,
            'location': self.start.location.id,
            'categories': [l.id for l in self.start.categories],
            'products': [l.id for l in self.start.products],
        }
        return action, data


class WarehouseKardexReport(Report):
    "Warehouse Kardex Report"
    __name__ = 'stock_co.warehouse_kardex_stock.report'

    @classmethod
    def get_by_move(cls, data, to_location, from_location, input_location):
        pool = Pool()
        Move = pool.get('stock.move')
        in_products = cls._get_str_products(data)
        origins = {}
        target_origins = [
            'stock.shipment.in', 'stock.shipment.out', 'stock.shipment.internal'
        ]
        target_origins.extend(Move._get_origin())
        for md in target_origins:
            origins[md] = {
                '_model': pool.get(md),
                'records': [],
                'fields': MAP_FIELDS.get(md, None)
            }

        print('origins...', origins)
        args = {
            'from_date': data['from_date'],
            'to_date': data['to_date'],
            'in_products': in_products,
            'to_location': to_location,
            'from_location': from_location,
            'location_id': from_location,
            'input_location': input_location,
        }
        df_start = polar_query(start_kardex, args)
        start = df_start.rows_by_key(key=['product_id'], unique=True)
        df_cost = polar_query(cost_kardex, args)
        df = polar_query(by_move_kardex, args)
        result = {}
        extra = []

        def _get_field(rel, fields, name):
            relations_field = fields[name].split('.')
            name_field = relations_field.pop()
            parent = rel
            for val in relations_field:
                parent = parent[val + '.']
            return parent[name_field]

        for model, value in origins.items():
            Model = value['_model']
            rec_origin = df.filter(
                pol.col('model')==model).select(pol.col('origin_id')
            )
            fields_ = value['fields']
            if not fields_:
                continue
            rec_ref = [row[0] for row in rec_origin.rows()]
            related_recs = Model.search_read(
                domain=[
                    ('id', 'in', rec_ref)
                ],  fields_names=list(fields_.values())
            )
            origins = []
            numbers = []
            parties = []
            for rel in related_recs:
                origin_id = rel['id']
                origins.append(f'{model},{origin_id}')
                numbers.append(_get_field(rel, fields_, 'number'))
                party_name = ''
                if 'party' in fields_:
                    party_name = _get_field(rel, fields_, 'party')
                parties.append(party_name)

            if origins:
                df_rel = pol.from_dict(
                    {'origin': origins, 'number': numbers, 'party': parties}
                )
                extra.append(df_rel)
        moves = df
        if extra:
            df_extra = pol.concat(extra, how='vertical')
            moves = moves.join(df_extra, on='origin', how='left')

        res = {}

        def _get_moves(rec):
            product_id = rec['product_id']
            df_ = moves.filter(pol.col('product_id')==product_id)
            product_balance = start.get(product_id, 0)
            balance = float(product_balance[0]) if isinstance(product_balance, tuple) else float(0)
            res = df_.rows(named=True)
            for row in res:
                cost_value = 0
                if row['output']:
                    _values = df_cost.filter(
                        (pol.col('product')==product_id) & (pol.col('effective_date') < row['effective_date'])).select(pol.last('cost_price')
                    )
                    row_cost = _values.rows()
                    cost_value = row_cost[0][0]
                row['start_balance'] = balance
                end_balance = balance + (row['input'] or 0) - (row['output'] or 0)
                row['end_balance'] = end_balance
                row['cost_price'] = cost_value
                balance = end_balance
            return res

        for rec in df.iter_rows(named=True):
            res[rec['product_id']] = {
                'code': rec['code'],
                'product': rec['product'],
                'reference': rec['reference'],
                'moves': _get_moves(rec),
            }
        return res.values()

    @classmethod
    def _get_str_products(cls, data):
        products = ''
        product_ids = data['products']
        if product_ids:
            str_products = ','.join(map(str, product_ids))
            products = f' AND product IN ({str_products}) '
        return products

    @classmethod
    def get_consolidated(cls, data, location_id):
        in_products = cls._get_str_products(data)
        args = {
            'from_date': data['from_date'],
            'to_date': data['to_date'],
            'in_products': in_products,
            'location_id': location_id,
        }
        res = polar_query(consolidated_kardex, args)
        return res.rows(named=True)

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Company = pool.get('company.company')
        Product = pool.get('product.product')
        Location = pool.get('stock.location')
        warehouse, = Location.browse([data['location']])

        if data['categories']:
            dom = [
                ('active', '=', True),
                ('template.active', '=', True),
                ('type', '=', 'goods'),
                ('consumable', '=', False),
            ]
            dom.append(['AND', ['OR', [
                ('account_category', 'in', data['categories']),
                ], [
                    ('categories', 'in', data['categories']),
                ],
            ]])
            data['products'] = (p['id'] for p in Product.search_read(dom))

        location_id = warehouse.storage_location.id
        input_location = warehouse.input_location.id
        if data['kind'] == 'consolidated':
            records = cls.get_consolidated(data, location_id)
        else:
            records = cls.get_by_move(data, location_id, location_id, input_location)

        report_context['records'] = records
        report_context['warehouse'] = warehouse.name
        report_context['company'] = Company(data['company'])
        return report_context


class CreateOrderPointStart(ModelView):
    "Create Order Point Start"
    __name__ = 'stock_co.create_order_point.start'
    category = fields.Many2One('product.category', 'Category', required=True,
                               domain=[('accounting', '=', False)])
    warehouse_location = fields.Many2One('stock.location',
        'Warehouse Location',
        domain=[('type', '=', 'warehouse')],
        states={
            'invisible': Not(Equal(Eval('type'), 'purchase')),
            'required': Equal(Eval('type'), 'purchase'),
            },
        depends=['type'])
    storage_location = fields.Many2One('stock.location', 'Storage Location',
        domain=[('type', '=', 'storage')],
        states={
            'invisible': Not(Equal(Eval('type'), 'internal')),
            'required': Equal(Eval('type'), 'internal'),
        },
        depends=['type'])
    location = fields.Function(fields.Many2One('stock.location', 'Location'),
            'get_location', searcher='search_location')
    provisioning_location = fields.Many2One(
        'stock.location', 'Provisioning Location',
        domain=[('type', 'in', ['storage', 'view'])],
        states={
            'invisible': Not(Equal(Eval('type'), 'internal')),
            'required': ((Eval('type') == 'internal')
                & (Eval('min_quantity', None) != None)),  # noqa: E711
        },
        depends=['type', 'min_quantity'])
    overflowing_location = fields.Many2One(
        'stock.location', 'Overflowing Location',
        domain=[('type', 'in', ['storage', 'view'])],
        states={
            'invisible': Eval('type') != 'internal',
            'required': ((Eval('type') == 'internal')
                & (Eval('max_quantity', None) != None)),  # noqa: E711
            },
        depends=['type', 'max_quantity'])
    type = fields.Selection(
        [('internal', 'Internal'),
         ('purchase', 'Purchase')],
        'Type', required=True)
    min_quantity = fields.Float('Minimal Quantity',
        digits=(16, Eval('unit_digits', 2)),
        states={
            # required for purchase and production types
            'required': Eval('type') != 'internal',
            },
        domain=['OR',
            ('min_quantity', '=', None),
            ('min_quantity', '<=', Eval('target_quantity', 0)),
            ],
        depends=['unit_digits', 'target_quantity', 'type'])
    target_quantity = fields.Float('Target Quantity', required=True,
        digits=(16, Eval('unit_digits', 2)),
        domain=[
            ['OR',
                ('min_quantity', '=', None),
                ('target_quantity', '>=', Eval('min_quantity', 0)),
                ],
            ['OR',
                ('max_quantity', '=', None),
                ('target_quantity', '<=', Eval('max_quantity', 0)),
                ],
            ],
        depends=['unit_digits', 'min_quantity', 'max_quantity'])
    max_quantity = fields.Float('Maximal Quantity',
        digits=(16, Eval('unit_digits', 2)),
        states={
            'invisible': Eval('type') != 'internal',
            },
        domain=['OR',
            ('max_quantity', '=', None),
            ('max_quantity', '>=', Eval('target_quantity', 0)),
            ],
        depends=['unit_digits', 'type', 'target_quantity'])
    company = fields.Many2One('company.company', 'Company', required=True,
            domain=[
                ('id', If(In('company', Eval('context', {})), '=', '!='),
                    Eval('context', {}).get('company', -1)),
            ])
    unit = fields.Function(fields.Many2One('product.uom', 'Unit'), 'get_unit')
    unit_digits = fields.Function(fields.Integer('Unit Digits'),
            'get_unit_digits')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_type():
        return "purchase"


class CreateOrderPoint(Wizard):
    "Create Order Point"
    __name__ = 'stock_co.create_order_point'
    start = StateView('stock_co.create_order_point.start',
        'stock_co.create_order_point_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'accept', 'tryton-ok', default=True),
        ])
    accept = StateTransition()

    def transition_accept(self):
        pool = Pool()
        OrderPoint = pool.get('stock.order_point')
        Product = pool.get('product.product')
        order_points = []
        type = self.start.type
        company = self.start.company
        warehouse_location = self.start.warehouse_location
        min_quantity = self.start.min_quantity
        target_quantity = self.start.target_quantity
        products = Product.search([('categories', 'in', [self.start.category])])
        # print(products)
        for product in products:
            order_points.append({
                'type': type,
                'company': company,
                'warehouse_location': warehouse_location.id,
                'product': product.id,
                'min_quantity': min_quantity,
                'target_quantity': target_quantity,
            })
        OrderPoint.create(order_points)
        return 'end'
