# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, Workflow, fields
from trytond.modules.company.model import employee_field
from trytond.pool import PoolMeta
from trytond.pyson import Eval, If


class CreateInventoriesStart(metaclass=PoolMeta):
    __name__ = 'stock.inventory.create.start'

    assign_to = employee_field('Assing To')
    category = fields.Many2One('product.category', 'Category', 'Product Categories')
    products = fields.Many2Many('product.product', None, None, 'Products',
        domain=[
            If(
                Eval('category'),
                ('template.categories', '=', Eval('category')),
                ()),
            ],
        depends=['categories'])


class CreateInventories(metaclass=PoolMeta):
    __name__ = 'stock.inventory.create'

    def get_inventory(self, location, Inventory):
        inventory = super(CreateInventories, self).get_inventory(
            location, Inventory)
        if self.start.products:
            self.start.complete_lines = False
            products = [s.id for s in self.start.products]
            prd_ids = [{'product': p} for p in products]
            inventory.assign_to = self.start.assign_to
            inventory.lines = prd_ids

        return inventory


class InventoryLine(metaclass=PoolMeta):
    "Stock Inventory Line"
    __name__ = 'stock.inventory.line'
    difference_quantity = fields.Function(fields.Float('Difference Quantity'),
        'get_difference_quantity')

    @classmethod
    def __setup__(cls):
        super(InventoryLine, cls).__setup__()
        cls._states = {
            'readonly': ~Eval('inventory_state').in_([
                "draft", "checkup", "pre_count"]),
            }
        cls.quantity.states = cls._states

    def get_difference_quantity(self, name=None):
        expected_quantity = self.expected_quantity or 0
        quantity = self.quantity or 0
        difference_quantity = 0
        if expected_quantity > 0:
            difference_quantity = expected_quantity - quantity
            difference_quantity = difference_quantity if difference_quantity == 0 else -difference_quantity
        else:
            difference_quantity = quantity - expected_quantity
        return difference_quantity


class Inventory(metaclass=PoolMeta):
    "Stock Inventory"
    __name__ = 'stock.inventory'

    assign_to = employee_field('Assing To')
    approved_by = employee_field("Approved By")
    comment = fields.Text('Comment')
    complete_line = fields.Boolean(
        "Complete",
        help="Add an inventory line for each missing product.")

    @classmethod
    def __setup__(cls):
        super(Inventory, cls).__setup__()
        cls.state.selection.extend([
            ('pre_count', 'Pre-count'),
            ('checkup', 'Checkup')])
        cls._states = {
            'readonly': (
                ~Eval('state').in_(["draft", "checkup", "pre_count"])
                | ~Eval('location')
                | ~Eval('date')),
            }
        cls.lines.states = cls._states
        cls._transitions |= set((
                ('draft', 'checkup'),
                ('checkup', 'draft'),
                ('checkup', 'pre_count'),
                ('pre_count', 'done'),
                ('pre_count', 'draft'),
                ('pre_count', 'cancelled'),
                ('draft', 'cancelled'),
                ))
        cls._buttons.update({
            'pre_count': {
                'invisible': Eval('state') != 'checkup',
                'depends': ['state'],
                },
            'checkup': {
                'invisible': Eval('state') != 'draft',
                'depends': ['state'],
                },
            'draft': {
                'invisible': ~Eval('state').in_(['pre_count', 'checkup']),
                'depends': ['state'],
                },
        })

    @classmethod
    @ModelView.button
    @Workflow.transition('pre_count')
    def pre_count(cls, inventories):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('checkup')
    def checkup(cls, inventories):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, inventories):
        pass

    @classmethod
    @ModelView.button_action('stock.wizard_inventory_count')
    def count(cls, inventories):
        complete_inv = {'yes': [], 'no': []}
        for inventory in inventories:
            if inventory.complete_line:
                complete_inv['yes'].append(inventory)
            else:
                complete_inv['no'].append(inventory)
        cls.complete_lines(complete_inv['yes'], fill=True)
        cls.complete_lines(complete_inv['no'], fill=False)

# class InventoryProductCategory(ModelSQL):
#     "Inventory -Product Category"
#     __name__ = "stock.inventory.product_category"
#     _table = 'stock_inventory_product_category_rel'
#     inventory = fields.Many2One('stock.inventory', 'Inventory',
#         ondelete='CASCADE', required=True)
#     category = fields.Many2One('product.category', 'Product Category',
#         ondelete='CASCADE', required=True)
