-- KARDEX CONSOLIDATED
SELECT
    mo.product AS id,
    pr.code AS code,
    tm.name AS name,
    tm.reference,
    uom.symbol AS unit,
    ROUND(SUM(CASE
        WHEN to_location=16 AND effective_date<'2024-06-01' THEN mo.quantity
        WHEN from_location=16 AND effective_date<'2024-06-01' THEN -mo.quantity END)::NUMERIC, 2
      ) AS start_balance,
    ROUND(SUM(
        CASE WHEN to_location=16 AND effective_date>='2024-06-01' THEN mo.quantity ELSE 0 END)::NUMERIC, 2
      ) AS input,
    ROUND(SUM(
        CASE WHEN from_location=16 AND effective_date>='2024-06-01' THEN mo.quantity ELSE 0 END)::NUMERIC
      ) AS output,
    ROUND(SUM(CASE
        WHEN to_location=16 AND effective_date<='2024-06-30' THEN mo.quantity
        WHEN from_location=16 AND effective_date<='2024-06-30' THEN -mo.quantity END)::NUMERIC, 2
    ) AS end_balance
FROM stock_move AS mo
JOIN product_product AS pr ON pr.id=mo.product
JOIN product_template AS tm ON pr.template=tm.id
JOIN product_uom AS uom ON tm.default_uom=uom.id
WHERE effective_date<='2024-06-30' AND state IN ('assigned', 'done')
  AND (to_location IN (16) OR from_location IN (16))
  GROUP BY mo.product, pr.code, tm.name, tm.reference, uom.symbol
  ORDER BY tm.name


-- COMPUTE START BALANCE FOR KARDEX
SELECT
    mo.product AS id,
    ROUND(SUM(CASE
        WHEN to_location=16 AND effective_date<'2024-06-01' THEN mo.quantity
        WHEN from_location=16 AND effective_date<'2024-06-01' THEN -mo.quantity END)::NUMERIC, 2
      ) AS start_balance
FROM stock_move AS mo
WHERE effective_date<'2024-06-01' AND state IN ('assigned', 'done') AND product IN (1120, 1106)
  AND (to_location=16 OR from_location=16)
  GROUP BY mo.product


-- KARDEX MOVE BY PRODUCT
SELECT
    mo.id AS _id,
    mo.product AS product_id,
    tm.name AS name,
    tm.reference,
    mo.effective_date AS effective_date,
    mo.cost_price AS cost_price,
    mo.unit_price AS unit_price,
    uom.symbol AS unit,
    mo.shipment AS shipment,
    SPLIT_PART(mo.origin, ',', 1) AS model,
    SPLIT_PART(mo.origin, ',', 2) AS origin_id,
    0 AS start_balance,
    CASE WHEN to_location=16 THEN mo.quantity ELSE 0 END AS input,
    CASE WHEN from_location=16 THEN mo.quantity ELSE 0 END AS output,
    0 AS end_balance
FROM stock_move AS mo
JOIN product_product AS pr ON pr.id=mo.product
JOIN product_template AS tm ON pr.template=tm.id
JOIN product_uom AS uom ON tm.default_uom=uom.id
WHERE effective_date>='2024-05-01' AND effective_date<='2024-06-30' AND (
    to_location=16 OR from_location=16)
    AND state IN ('assigned', 'done') AND product IN (1120, 1106)
ORDER BY mo.effective_date ASC, mo.id ASC


-- COMPUTE PRODUCT COST FOR KARDEX
SELECT
    product,
    cost_price,
    effective_date,
FROM product_average_cost AS avg
WHERE effective_date<'2024-06-01' AND product IN (1120, 1106)


-- MERGE ORIGINS STOCK MOVE vs PURCHASE LINE
SELECT
  ms.id,
  mo.origin,
  mo.product
FROM stock_move AS mo
INNER JOIN (
  SELECT
    id AS id,
    SPLIT_PART(origin, ',', 2)::INTEGER AS rel_id,
    product AS product,
    shipment AS shipment
  FROM stock_move
  WHERE origin ilike 'stock.move,%' AND product IN (1120, 1106)
) ms ON ms.rel_id=mo.id;

-- TEMPORAL TEST
SELECT
    mo.id AS _id,
    mo.product AS product_id,
    -- tm.name AS name,
    -- tm.reference,
    mo.effective_date AS effective_date,
    orig.cost AS input_cost,
    CASE WHEN mo.origin ILIKE 'sale.line%' THEN mo.unit_price ELSE 0 END AS output_cost,
    -- mo.cost_price AS cost_price,
    -- mo.unit_price AS unit_price,
    uom.symbol AS unit,
    CASE WHEN shipment IS NULL THEN mo.origin ELSE mo.shipment END AS origin,
    CASE WHEN shipment IS NULL THEN SPLIT_PART(mo.origin, ',', 1) ELSE SPLIT_PART(mo.shipment, ',', 1) END AS model,
    CASE WHEN shipment IS NULL THEN SPLIT_PART(mo.origin, ',', 2) ELSE SPLIT_PART(mo.shipment, ',', 2) END AS origin_id,
    0 AS start_balance,
    CASE WHEN to_location=16 THEN mo.quantity ELSE 0 END AS input,
    CASE WHEN from_location=16 THEN mo.quantity ELSE 0 END AS output,
    0 AS end_balance
FROM stock_move AS mo
JOIN product_product AS pr ON pr.id=mo.product
JOIN product_template AS tm ON pr.template=tm.id
JOIN product_uom AS uom ON tm.default_uom=uom.id
LEFT JOIN (
  SELECT
    origin,
    unit_price AS cost,
    'stock.move,' || id AS origin_id
  FROM stock_move
  WHERE effective_date>='2024-05-01' AND effective_date<='2024-06-30' AND to_location=14
      AND state IN ('assigned', 'done') AND product IN (1120, 1169)
) AS orig ON orig.origin_id=mo.origin
WHERE effective_date>='2024-05-01' AND effective_date<='2024-06-30' AND (
    to_location=16 OR from_location=16)
    AND state IN ('assigned', 'done') AND product IN (1120, 1169)
ORDER BY mo.effective_date ASC, mo.id ASC;
