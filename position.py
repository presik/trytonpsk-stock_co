# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.i18n import gettext
from trytond.model import ModelSQL, ModelView, Unique, fields
from trytond.pool import Pool

from .exceptions import DeleteWarning


class ProductPosition(ModelSQL, ModelView):
    "Product Position"
    __name__ = "product.position"
    name = fields.Char('Name', required=True)
    code = fields.Char('Code')
    warehouse = fields.Many2One('stock.location', 'Warehouse', states={
        'required': True,
        }, domain=[('type', '=', 'warehouse')],
    )

    @classmethod
    def __setup__(cls):
        super(ProductPosition, cls).__setup__()
        cls._order.insert(0, ('name', 'ASC'))
        t = cls.__table__()
        cls._sql_constraints = [
            ('unique', Unique(t, t.code), f"Code must be unique. {t.code}"),
            ]

    @classmethod
    def search_rec_name(cls, name, clause):
        clause = tuple(clause)
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
            ('name',) + clause[1:],
            ('code',) + clause[1:],
            (cls._rec_name,) + clause[1:],
            ]

    @classmethod
    def delete(cls, records):
        Warning = Pool().get('res.user.warning')
        ProductTemplate = Pool().get('product_template.position')
        for record in records:
            key = '%s.delete' % record
            pds_position = ProductTemplate.search([('position', '=', record.id)])
            if Warning.check(key) and len(pds_position) > 0:
                raise DeleteWarning(key,
                    gettext('stock_co.msg_delete_position',
                        position=record.rec_name, count=len(pds_position)))
        super(cls, ProductPosition).delete(records)


class ProductTemplatePosition(ModelSQL, ModelView):
    "Product Template Position"
    __name__ = 'product_template.position'

    # _rec_name = 'position'
    position = fields.Many2One('product.position', 'Position', required=True,
        ondelete='CASCADE')
    template = fields.Many2One('product.template', 'Template', required=True,
        ondelete='RESTRICT')
    warehouse = fields.Function(fields.Many2One('stock.location', 'Warehouse'),
        'on_change_with_warehouse')

    @fields.depends('position')
    def on_change_with_warehouse(self, name=None):
        warehouse = None
        if self.position:
            warehouse = self.position.warehouse.id
        return warehouse

    @classmethod
    def search_rec_name(cls, name, clause):
        clause = tuple(clause)
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
            ('position.name',) + clause[1:],
            ('position.code',) + clause[1:],
        ]
